const config = angular.module('config', [])


config.constant('PAGES', [
  {
    name: 'users', label: 'Users', url: './templates/users.html',
    child: [{ name: 'tasks', url: './templates/tasks.html' }]
  },
  { name: 'settings', label: 'Settings', url: './templates/settings.html' },
])


config.constant('SERVICES', [
  { name: 'users', url: 'http://localhost:3000/users/' },
  { name: 'tasks', url: 'http://localhost:3000/tasks/' },

])

