const app = angular.module('myApp', ['tasks', 'users', 'auth', 'config', 'settings', "ui.router"])

// app.run(($rootScope) => {})

app.controller('AppControl', ($scope, PAGES) => {

    const val = $scope.appScope = {}

    val.pages = PAGES
    val.currentPage = val.pages[0]

    val.goToPage = (pageName) => {
        val.currentPage = val.pages.find(p => p.name === pageName)
    }

})

app.config(function ($stateProvider) {
    $stateProvider
        .state({ name: 'users', url: './templates/users.html', template: 'Users', })
        .state({ name: 'settings', url: './templates/settings.html', template: 'Settings', })

})



angular.bootstrap(document, ['myApp'])