const settings = angular.module('settings', [])

settings.controller('SettingsControl', $scope => { })

settings.component('taskPage', {
    /*html*/
    template:
        `<div>
            <h1>Task Page </h1>
            <div class="row">
                <div class="col"><task-list on-select="$ctrl.getTask($event)" tasks="$ctrl.tasks"></task-list></div>
                <div class="col"><task-details on-edit-task="$ctrl.updateTask($event)" task="$ctrl.selectedTask" ></task-details></div>
            </div>
        </div>`,
    controller() {
        this.title = 'Task'

        this.tasks = [
            { id: 1, name: 'One' },
            { id: 2, name: 'Two' },
            { id: 3, name: 'Ten' }
        ]
        this.selectedTask = {}

        this.getTask = taskData => {
            this.selectedTask = taskData
        }
        this.updateTask = taskData => {
            let index = this.tasks.findIndex(item => item.id === taskData.id)
            this.tasks[index] = taskData
            this.selectedTask = taskData
        }
    }

})

settings.component('taskList', {

    bindings: {
        tasks: '<',
        onSelect: '&'
    },
    /*html*/
    template:
        `<div>
            <h1>Task List </h1>
            <div class="list-group" ng-repeat="item in $ctrl.tasks track by $index"> 
                <div 
                    ng-class="{active: item.id === $ctrl.selectedTask.id}" 
                    ng-click="$ctrl.onSelect({$event: item}); $ctrl.selectedTask = item" 
                    class="list-group-item">
                    {{item.name}}
                </div>
            </div>
        </div>`,
})

settings.component('taskDetails', {

    bindings: {
        task: '<',
        onEditTask: '&'
    },

    /*template*/
    template: `<div>
            <h1>Task Details</h1>
            <div class="d-flex">
                <p class="m-1"><strong>Task Id:</strong> </p> 
                <p class="m-1">{{$ctrl.task.id}}</p>
            </div>
            <div class="d-flex">
                <p class="m-1"><strong>Task name:</strong></p>
                <p class="m-1" ng-if="!$ctrl.edit" > {{$ctrl.task.name}} </p> <input ng-if="$ctrl.edit" ng-model="$ctrl.draft.name" />
            </div>
            <button ng-class="{disabled: !$ctrl.task.id };" ng-click="$ctrl.changeStatus() " type="button" class="btn btn-primary m-1">{{$ctrl.edit | editStatus}}</button>
            <button ng-if="$ctrl.edit" ng-click="$ctrl.save(); $ctrl.changeStatus()" type="button" class="btn btn-primary m-1">Save</button>

        </div>`,

    controller() {
        this.edit = false
        this.changeStatus = () => {
            this.edit = !this.edit
        }

        this.$onChanges = () => {
            this.draft = { ...this.task }
        }

        this.save = () => {
            this.onEditTask({ $event: this.draft })
        }
    }
})

settings.filter('editStatus', function () {
    return function (value) {
        return value ? 'Cancel' : 'Edit'
    }
})

settings.directive('appAlert', () => {
    return {
        scope: {
            type: '@',
            message: '@',
            onDissmis: '&'
        },
        transclude: true,
        template: /*html*/ `
  
            <div 
            ng-if="appAlert.open"
            class="alert alert-dismissible alert-{{appAlert.type}} fade show" role="alert">
            <ng-transclude>{{appAlert.message}}</ng-transclude>
            <span ng-click="appAlert.close()" class="close float-end">&times;</span>

          </div>
            
        `,
        bindToController: true,
        controllerAs: 'appAlert',

        controller($scope) {
            this.open = true
            this.close = () => {
                this.onDissmis()
                this.open = !this.open
            }
        }
    }
})
