const task = angular.module('tasks', ['tasks.service'])

task.controller('TaskControl', ($scope, $http, TasksService ) => {

    $scope.tasks = []
    $scope.editName = 'Edit'
    $scope.findtask = ''
    $scope.countTask = 0
    $scope.newTaskName = ''

    $scope.refreshTasks = () => {

        $http.get("http://localhost:3000/tasks")
            .then(res => $scope.tasks = res.data)
            .then(() => $scope.countTask = $scope.tasks.filter(item => item.completed === true).length)
    }


    $scope.refreshTasks()

    //Wyłapywanie zmiany. 
    // $scope.$watchCollection('tasks', (newVal, oldVal) =>{ 
    //     $scope.refreshTasks()
    // })

    $scope.task = $scope.tasks[0]

    $scope.taskFilter = (filters, param = 'name_like') => {
        $http.get("http://localhost:3000/tasks/?" + param + '=' + filters)
            .then(res => $scope.tasks = res.data)
            // .then(() => $scope.countTask = $scope.tasks.filter(item => item.completed === true).length)
    }

    $scope.selectTask = (id) => {
        $scope.task = $scope.tasks.find(item => item.id === id)
        $scope.draft = { ...$scope.task }
    }

    $scope.editTask = () => {
        $scope.editName = $scope.editName === 'Edit' ? "Cancel" : 'Edit'
        $scope.draft = { ...$scope.task }
    }

    $scope.saveTask = () => {
        let index = $scope.tasks.findIndex(item => item.id === $scope.draft.id)
        $scope.tasks[index] = { ...$scope.draft }
        $scope.task = $scope.tasks[index]
        $http.put('http://localhost:3000/tasks/' + $scope.draft.id, $scope.draft)
        $scope.refreshTasks()
    }

    $scope.clearTask = () => {
        $scope.draft = {
            ...$scope.draft,
            name: '',
            completed: false
        }
    }

    $scope.deleteTask = () => {
        $http.delete('http://localhost:3000/tasks/' + $scope.task.id)
        $scope.refreshTasks()
    }

    $scope.addTaskEnter = (e) => {
        if (e.which === 13) $scope.addTask()
    }

    $scope.addTask = () => {
        let newTask = {
            id: (Math.random() + 1).toString(36).substring(7),
            name: $scope.newTaskName,
            completed: false,
        }
        $http.post('http://localhost:3000/tasks/', newTask)
        $scope.refreshTasks()
    }
})