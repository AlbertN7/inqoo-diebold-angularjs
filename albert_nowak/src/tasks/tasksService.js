const tasksService = angular.module('tasks.service', [])

tasksService.service('TasksService', function ($http, SERVICES) {

    this.tasksURL = SERVICES.find(res => res.name === 'tasks').url

    this.getTasks = (userId) => {
        let filtr = '?userId='
        return $http.get(this.tasksURL + filtr + userId)
    }


    // this.updateUser = (userID, data) => {
    //     return $http.put(this.usersURL + userID, data)
    // }

    // this.deleteUser = (userID) => {
    //     return $http.delete(this.usersURL + userID)
    // }

    // this.addUser = (userData) => {
    //     return $http.post(this.usersURL, userData)
    // }

    // this.getUserById = (userID) => {
    //     return $http.get(this.usersURL + userID)
    // }

    // this.findUserByName = (userName) => {
    //     let filtr = '?name_like='
    //     return $http.get(this.usersURL + filtr + userName)
    // }

    // this.sortUsers = (val) => {
    //     let filtr = '?_sort='
    //     return $http.get(this.usersURL + filtr + val)
    // }

    // this.signInUser = (emial) => {
    //     let filtr = '?email='
    //     return $http.get(this.usersURL + filtr + emial)
    // }

})