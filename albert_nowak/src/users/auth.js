const auth = angular.module('auth', ['users.service'])

auth.controller('AuthControl', ($scope, $timeout, UsersService) => {

    $scope.authStatus = false
    $scope.authUser = []
    $scope.singInEmail = 'karol@inqoo.pl'
    $scope.authMessage = { status: 'wait', message: '' }

    // $scope.$watchCollection(()=> $scope.authUser, (newVal, oldVal) => {

    // })

    $scope.loginUser = (email) => {
        UsersService.signInUser(email)
            .then(res => {
                try {
                    if (res.data[0].email === email) {
                        $scope.authStatus = true
                        $scope.authUser = res.data[0]
                        $scope.singInEmail = ''
                        $scope.authAlert('signIn', `Hello ${$scope.authUser.name}, nice to see you.`)
                    }
                }
                catch {
                    $scope.authStatus = false
                    $scope.authAlert('badEmial', `This emial ${email} is bad, Try again...`)
                }
            })
    }

    $scope.loginUser($scope.singInEmail)

    $scope.logout = () => {
        $scope.authStatus = false
        $scope.authUser = []
        $scope.singInEmail = 'karol@inqoo.pl'
        $scope.authAlert('signOut', 'Successfully sign out.')
    }

    $scope.authAlert = (status, message) => {
        $scope.authMessage.status = status
        $scope.authMessage.message = message
        if ($scope.authMessage.status !== 'wait') {
            $timeout(() => {
                $scope.authAlert('wait', '')
            }, 3000);
        }
    }
})