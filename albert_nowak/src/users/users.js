const users = angular.module('users', ['users.service'])


users.component('userPage', {
    /*html*/
    template:
        `<div>
            <h1>Users</h1>
            <div class="row">
                <div class="col"><users-list on-select="$ctrl.getUser($event)" users="$ctrl.users"></users-list></div>
                <div class="col">
                    <users-details on-edit-user="$ctrl.updateUser($event)" user="$ctrl.user" ></users-details>
                    <delete-user on-delete-user="$ctrl.deleteUser($event)" user="$ctrl.user" ></delete-user>

                </div>
            </div>
            <add-user on-add-user="$ctrl.addUser($event)" ></add-user>
        </div>`,

    controller(UsersService) {

        this.users = []
        this.user = {}
        this.editUserStatus = false
        this.userMessage = { status: 'wait', message: '' }

        this.editUserData = []

        let getUsersData = () => {
            UsersService.getUsers()
                .then(res => this.users = res.data)
        }
        getUsersData()

        this.getUser = (selectedUser) => {
            this.user = selectedUser
        }

        this.updateUser = userData => {
            UsersService.updateUser(userData.id, userData)
                .then(() => {
                    getUsersData()
                    this.user = userData
                })
        }
        this.addUser = (userData) => {
            UsersService.addUser(userData)
                .then(() => { getUsersData() })
        }

        this.deleteUser = () => {
            UsersService.deleteUser(this.user.id)
                .then(() => {
                    getUsersData()
                    this.user = {}
                })
        }
    }
})

settings.component('deleteUser', {
    bindings: {
        onDeleteUser: '&',
        users: '<',
    },
    /*html*/
    template:
        `<div>
            <button ng-click="$ctrl.save()" type="button" class="btn btn-danger m-1">Delete</button>
            </div>
        </div>`,

    controller() {
        this.save = () => {
            this.onDeleteUser({ $event: this.users })
        }
    }
})

settings.component('addUser', {

    bindings: {
        onAddUser: '&',
    },
    /*html*/
    template:
        `<div>
            <button ng-click="$ctrl.changeStatus() " type="button" class="btn btn-info m-1">{{$ctrl.add? 'Cancel': 'Add'}}</button>
            <div ng-if="$ctrl.add">
            
                <div class="d-flex">
                    <p class="m-1"><strong>User name:</strong></p>
                    <input ng-model="$ctrl.newUser.name" />
                </div>
                <div class="d-flex">
                    <p class="m-1"><strong>User email:</strong></p>
                    <input  ng-model="$ctrl.newUser.email" />
                </div>
                <button  ng-click="$ctrl.save()" type="button" class="btn btn-success m-1">Save</button>

            </div>
        </div>`,

    controller() {
        this.add = false

        let createNewUser = () => {
            this.newUser = {
                id: '',
                name: '',
                email: '',
                admin: false
            }
        }

        this.changeStatus = () => {
            this.add = !this.add
            createNewUser()

        }

        this.save = () => {
            this.onAddUser({ $event: this.newUser })
            this.changeStatus()
        }
    }
})

settings.component('usersList', {

    bindings: {
        users: '<',
        onSelect: '&'
    },
    /*html*/
    template:
        `<div>
            <h2>User list </h2>
            <div class="list-group" ng-repeat="item in $ctrl.users track by $index"> 
                <div 
                    ng-class="{active: item.id === $ctrl.selectedUser.id}" 
                    ng-click="$ctrl.onSelect({$event: item}); $ctrl.selectedUser = item" 
                    class="list-group-item">
                    {{item.name}}
                </div>
            </div>
        </div>`,
})

settings.component('usersDetails', {

    bindings: {
        user: '<',
        onEditUser: '&'
    },

    /*template*/
    template: `<div>
            <h2>User Details</h2>
            <div class="d-flex">
                <p class="m-1"><strong>User Id:</strong> </p> 
                <p class="m-1">{{$ctrl.user.id}}</p>
            </div>
            <div class="d-flex">
                <p class="m-1"><strong>User name:</strong></p>
                <p class="m-1" ng-if="!$ctrl.edit" > {{$ctrl.user.name}} </p> <input ng-if="$ctrl.edit" ng-model="$ctrl.draft.name" />
            </div>
            <div class="d-flex">
                <p class="m-1"><strong>User email:</strong></p>
                <p class="m-1" ng-if="!$ctrl.edit" > {{$ctrl.user.email}} </p> <input ng-if="$ctrl.edit" ng-model="$ctrl.draft.email" />
            </div>
            <button ng-click="$ctrl.changeStatus() " type="button" class="btn btn-warning m-1">{{$ctrl.edit | editStatus}}</button>
            <button ng-if="$ctrl.edit" ng-click="$ctrl.save(); $ctrl.changeStatus()" type="button" class="btn btn-success m-1">Save</button>

        </div>`,

    controller() {
        this.edit = false
        this.changeStatus = () => {
            this.edit = !this.edit
        }

        this.$onChanges = () => {
            this.draft = { ...this.user }
        }

        this.save = () => {
            this.onEditUser({ $event: this.draft })
        }
    }
})

settings.filter('editStatus', function () {
    return function (value) {
        return value ? 'Cancel' : 'Edit'
    }
})
