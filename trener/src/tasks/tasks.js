
const tasks = angular.module('tasks', [])


angular.module('tasks')
  .directive('tasksPage', function () {
    return {
      scope: {},
      template:/* html */`<div> TasksPage {{$ctrl.title}}
        <div class="row">
          <div class="col">
            <tasks-list tasks="$ctrl.tasks" 
                        on-select="$ctrl.select($event)"></tasks-list>
          </div>
          <div class="col">
            <task-details task="$ctrl.selected" on-cancel="$ctrl.cancel($event)">
            </task-details>
          </div>
        </div>
      </div>`,
      controller($scope) {
        const vm = $scope.$ctrl = {}
        vm.title = 'Tasks'
        vm.selected = null
        vm.tasks = [{ id: 1, name: 'task1' }, { id: 2, name: 'task2' }, { id: 3, name: 'task3' }]
        vm.select = (task) => { vm.selected = task }
        vm.cancel = (event) => { vm.selected = null }
      }
    }
  })
  .directive('tasksList', function () {
    return {
      scope: { tasks: '=', onSelect: "&" },
      template:/* html */`<div> List: <div class="list-group">
        <div class="list-group-item" ng-repeat="task in tasks" 
          ng-class="{active: task.id == selected.id}"
          ng-click="onSelect({$event: task}); $parent.selected = task">
        {{$index+1}}. {{task.name}}</div>
      </div></div>`
    }
  })
  .directive('taskDetails', function () {
    return {
      scope: { task: '=', onEdit: '&', onCancel: '&' },
      template:/* html */`<div>tasksDetails <dl>
        <dt>Name</dt><dd>{{task.name}}</dd>
      </dl>
        <button ng-click="onEdit({$event:task})">Edit</button>
        <button ng-click="onCancel({$event:task})">Cancel</button>
      </div>`
    }
  })














tasks.constant('INITIAL_TASKS', [])
tasks.constant('INITIAL_MODE', 'show')

/* 
https://docs.angularjs.org/api/auto/service/$injector

// inferred (only works if code not minified/obfuscated)
$injector.invoke(function(serviceA){});

// annotated
function explicit(serviceA) {};
explicit.$inject = ['serviceA'];
$injector.invoke(explicit);

// inline
$injector.invoke(['serviceA', function(serviceA){}]);
*/

tasks.controller('TasksCtrl', TasksCtrl)

TasksCtrl.$inject = ['$scope', 'INITIAL_TASKS', 'INITIAL_MODE']
function TasksCtrl($scope, INITIAL_TASKS, INITIAL_MODE) {

  console.log('Hello TasksCtrl', { ...$scope })

  $scope.mode = INITIAL_MODE
  $scope.tasks = INITIAL_TASKS
  $scope.filtered = []

  $scope.draft = {}

  $scope.selectedTask = {
    id: "123",
    name: "Task 123",
    completed: true
  }


  $scope.query = ''
  $scope.changeQuery = () => {
    $scope.filtered = $scope.tasks.filter(
      t => t.name.toLocaleLowerCase().includes($scope.query.toLocaleLowerCase())
    )
  }

  $scope.counts = { completed: 0, total: 0 }
  $scope.updateCounts = () => {
    $scope.counts = $scope.tasks.reduce((counts, task) => {
      counts.total += 1;
      if (task.completed) { counts.completed += 1 }
      return counts
    }, { completed: 0, total: 0 })
  }

  // shallow compares immutable values ( === )
  $scope.$watch('tasks', (newVal, oldVal) => {
    // $scope.$watchCollection('tasks', (newVal, oldVal) => {
    // debugger
    console.log(' task watcher update');
    $scope.changeQuery('')
    $scope.updateCounts()
  })
  // $scope.changeQuery('')

  $scope.edit = () => {
    $scope.mode = 'edit'
    // $scope.draft = $scope.selectedTask
    $scope.draft = { ...$scope.selectedTask }
  }

  $scope.cancel = () => {
    $scope.mode = 'show'
  }

  $scope.toggleCompleted = (draft) => {
    draft.completed = !draft.completed
    $scope.tasks = $scope.tasks.map(
      task => task.id == draft.id ? draft : task
    )
  }

  $scope.save = () => {
    $scope.mode = 'show'
    $scope.selectedTask = { ...$scope.draft }
    $scope.tasks = $scope.tasks.map(
      task => task.id == $scope.draft.id ? $scope.draft : task
    )
    // $scope.changeQuery($scope.query)
  }

  $scope.addTask = () => {
    // $scope.tasks.push({ // mutable wont trigger $watch!
    $scope.tasks = [...$scope.tasks, { // Immutable copy // can use simple $watch
      id: Date.now().toString(),
      name: $scope.fastTaskName, completed: false
    }]
    $scope.fastTaskName = ''
    // $scope.changeQuery($scope.query)
  }

  $scope.remove = (id) => {
    $scope.tasks = $scope.tasks.filter(task => task.id !== id)
    // $scope.changeQuery($scope.query)
  }

  $scope.select = (item) => {
    $scope.selectedTask = item
  }
}