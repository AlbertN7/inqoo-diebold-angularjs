
const users = angular.module('users', ['users.service'])

/**
 * @typedef User
 * @property {string} username
 */

/**
 * @typedef MyScope
 * @property {User} [selected]
 * @property {User} [draft]
 */

angular.module('users')
  .controller('UsersPageCtrl', [
    '$scope', '$timeout', '$q', 'UsersService',
    ($scope, $timeout, $q, UsersService) => {
      /** @type MyScope */
      const vm = $scope.usersPage = {}
      vm.selected = null
      vm.draft

      vm.refresh = () => {
        return UsersService.fetchUsers()
          .then((data) => vm.users = data)
      }
      vm.refresh()

      vm.create = () => {
        vm.selected = {}
        vm.draft = {}
        vm.mode = 'edit'
      }

      vm.select = (id) => {
        return UsersService.fetchUserById(id).then(user => {
          vm.selected = user
          vm.mode = 'details'
        })
      }

      vm.edit = () => {
        vm.mode = 'edit'
        vm.draft = { ...vm.selected }
      }
      vm.select('1').then(() => vm.edit())

      vm.save = (draft) => {
        if (vm.userForm.$invalid) {
          vm.showMessage('Form has errors')
          return
        }
        const result =
          draft.id ? UsersService.saveUpdatedUser(draft) : UsersService.saveNewUser(draft)

        return result.then(saved => {
          return $q.all([vm.select(saved.id), vm.refresh()])
        })
          .then(() => vm.showMessage('Changes Saved'))
      }

      vm.showMessage = msg => {
        vm.message = msg;
        $timeout(() => {
          vm.message = ''
        }, 2000)
      }

    }])
  // .controller('UserListCtrl', ($scope) => {
  //   const vm = $scope.list = {}
  //   vm.filtered = []
  //   vm.select = (id) => $scope.$emit('selectUser', id)

  // })
  // .controller('UserDetailsCtrl', ($scope) => {
  //   const vm = $scope.details = {}
  //   vm.user = null

  //   $scope.$on('userSelected', (event, user) => { vm.user = (user) })

  // })
  // .controller('UserEditFormCtrl', ($scope) => {
  //   const vm = $scope.editform = {}
  //   vm.draft = {}
  // })
